(function($){
  var fields = {
    "wdpa_id": {
      name: "WDPA ID",
      get_value: function(json_response_data){
        // let $obj = jQuery(json_response_data['designation']);
        // $obj.html(json_response_data['wdpa_id']);
        let $obj = jQuery(`<a href="/pa/${json_response_data['wdpa_id']}">${json_response_data['wdpa_id']}</a>`);
        return $obj;
      }
    },
    "region": {
      name: "Region",
      filter: {
        display_order: 3,
        label: "Region",
        default_option: "All Regions",
        default_value: "%",
        get_value: function(json_response_data){
          return $(json_response_data["region"]).text();
        },
        get_text: function(json_response_data){
          return $(json_response_data["region"]).text();
        }
      }
    },
    "iso3": {
      name: "ISO3",
      filter: {
        display_order: 4,
        label: "Country",
        default_option: "All Countries",
        default_value: "%",
        get_text: function(json_response_data){
          return $(json_response_data["country"]).text();
        }
      }
    },
    "country": {
      name: "Country",
    },
    "type": {
      name: "Type",
      filter: {
          display_order: 5,
          label: "Type",
          default_option: "All Types",
          default_value: "%",
          get_value: function(json_response_data){
            let type_desc = json_response_data["type"];
            let result = '0';
            switch(type_desc.toLowerCase()){
              case 'terrestrial':
                result = '0';
                break;
              case 'coastal':
                result = '1';
                break;
              case 'marine':
                result = '2';
                break;
            }
            return result;
          }
      }
    },
    "pa": {
      name: "Protected Area",
    },
    "designation": {
      name: "Designation",
      get_value: function(json_response_data){
        let $obj = jQuery(json_response_data['designation']);
        return $obj.html();
      }
    },
    "ass_method": {
      name: "Methodology",
      filter: {
          display_order: 1,
          label: "Tool",
          default_option: "All Methodologies",
          default_value: "%"
      }
    },
    "ass_year": {
      name: "Year",
      filter: {
          display_order: 2,
          label: "Year",
          default_option: "All Years",
          default_value: "%"
      }
    },
    "assessment_report": {
      name: "Report"
    },
    "ass_link": {
      name: "Link",
    },
    "source": {
      name: "Source",
      filter: {
          display_order: 6,
          label: "Source",
          default_option: "All Sources",
          default_value: "%"
      }
    },
    // "moderation_state": {
    //   name: "Status"
    // },
    "edit_link": {
      name: "Edit",
      removable: true
    },
  };

  biopamaAssessmentMap.initMap(
    function(){
      biopamaAssessmentTable.initAssessmentTable('/rest/gd_page', fields, biopamaAssessmentMap);
    }
  );

  Drupal.behaviors.PAGEFormTitleChange = {
    attach: function(context, settings){
      $(document).ready(function(){
        var $element = $(".ui-dialog-title").first();
        $element.text($element.text().replace('Create', 'Add'));
      });

		  $('#drupal-off-canvas').find('form.node-gd-page-assessment-form .alert-success, form.node-gd-page-assessment-edit-form .alert-success').once('updated-view').each( function() {
        $("div.ui-dialog-titlebar button.ui-dialog-titlebar-close").delay(800).trigger('click');
        biopamaAssessmentTable.setTableData();
	  	});
    }
  };
})(jQuery);