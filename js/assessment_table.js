/**
 * This component handles the generation of an assessment table (e.g., as in the /page_assessments module) to make it automatically generated, passing the columns structure 
 * through a json object which should contain innerly as many other objects as the columns of the desrired table.
 * Each of such objects should specify:
 * - Key: the id of the coulmn within the table; such id should be equal to the name of the field in the response object from the service called in order to populate the table.
 * - Object's properties:
 * 		- name: The name that will be displayed as column header in the table.
 * 		- filter: a json OPTIONAL defining the properties of the (optional) filter to be applied to the column
 * 			- display_order: a (optional) number defining the order the placement of the filter within the filters' list.
 * 			- label: The text that will be displayed besides the filter.
 * 			- default_option: The default selection item's text
 * 			- default_value: The default selection item's value ('%' for no choice)
 * 			- get_text: A function, having as argument the service's response data, that can be used in order to give the selection item a dynamic text.
 * 			- get_value: A function, having as argument the service's response data, that can be used in order to give the selection item a dynamic value.
 * 
 * The generation of the table (which exploits the DataTable.js library) can be started by invoking the exposed initAssessmentTable function.
 */

var biopamaAssessmentTable;

(function($){
	var table;
	var filters = {};
	var $spinner = $('#spinner');
	var assessmentMap;
	var tableDataServiceUrl;

	function setupClearButton(){
		$('#reset_filters').on('click',function(){
			var currentURL = document.location.href;
			if(currentURL.includes('?')) {
				currentURL = currentURL.split('?')[0];
				history.pushState({}, null, currentURL);
			}

			for(k in filters){
				filters[k] = null;
			}
			
			setTableData();
		});
	}

	function setupFilterChangeEvent(){
		$('.cql_filters').on(
			'change',
			function(){
				var currentURL = document.location.href;
				var parameter = $(this).attr('param');
				var value = $(this).val();
				if(currentURL.includes(parameter)) {
					currentURL = removeURLParameter(currentURL, parameter);
				}
				if(value == '%'){
					filters[parameter] = null;
				}
				else{
					filters[parameter] = value;
					qsAddSymbol = currentURL.includes('?')? '&' : '?';
					currentURL = `${currentURL}${qsAddSymbol}${parameter}=${value}`;
				}
				history.pushState({}, null, currentURL);
				setTableData();
			}
		);
	}
	
	function setupFilters(){
		var $filterContainer = $("#table-assessments-predefined-filters");
		var $filterLabel, $filterItem, $filterItemDefaultOption, sortedFilterItems = [];

		var sortFilterItems = function($f1, $f2){
			var order = 0;
			var f1do = Number($f1.data('displayOrder'));
			var f2do = Number($f2.data('displayOrder'));
			if(f1do < f2do){
				order = -1;
			}
			else if(f1do > f2do){
				order = 1;
			}
			return order;
		}

		var itemOrder;
		for(k in response_fields_to_table_map){
			if(response_fields_to_table_map[k].filter){
				itemOrder = response_fields_to_table_map[k].filter.display_order? response_fields_to_table_map[k].filter.display_order : 1000;

				$filterLabel = $(`<label data-display-order="${itemOrder}" for="${k}">${response_fields_to_table_map[k].filter.label}</label>`);
				sortedFilterItems.push($filterLabel);

				$filterItem = $(`<select data-display-order="${itemOrder}" class="form-control cql_filters" id="${k}" param="${k}" tabindex="-1">`);
				$filterItemDefaultOption = $(`<option value="${response_fields_to_table_map[k].filter.default_value}">${response_fields_to_table_map[k].filter.default_option}</option>`);
				$filterItem.append($filterItemDefaultOption);
				sortedFilterItems.push($filterItem);

				filters[k] = null;
			}
		}
		sortedFilterItems.sort(sortFilterItems);
		$filterContainer.append(sortedFilterItems);
		$filterContainer.append($('<button class="btn btn-info" type="button" id="reset_filters">Clear All</button>'));

		var url_string = new URL(window.location.href);
		for(k in filters){
			if(url_string.searchParams.get(k)){
				filters[k] = url_string.searchParams.get(k);
			}
		}

		setupClearButton();
		setupFilterChangeEvent();
	}

	function updateFilters(data){
		$.each($('.cql_filters'),function(){
			$(this).find('option').not(':first').remove();
		});

		var generalFilterList = {}, option_value, option_text;
		data.forEach((d, i) => {
			for(k in d){
				if(response_fields_to_table_map[k] && response_fields_to_table_map[k].filter){
					option_value = (response_fields_to_table_map[k].filter.get_value)? response_fields_to_table_map[k].filter.get_value(d) : d[k];
					option_text = (response_fields_to_table_map[k].filter.get_text)? response_fields_to_table_map[k].filter.get_text(d) : d[k];
					if(option_value && option_text){
						if(!generalFilterList[k]){
							generalFilterList[k] = {};
						}
						generalFilterList[k][option_text] = {value: option_value};
					}
				}
			}
		});

		var generalFilterList, sortedKey, valueObj;
		for(k in generalFilterList){
			generalFilterListSortedKeys = Object.keys(generalFilterList[k]).sort();
			for(i in generalFilterListSortedKeys){
				sortedKey = generalFilterListSortedKeys[i];
				valueObj = generalFilterList[k][sortedKey];
				$(`#${k}.cql_filters`).append($(`<option value="${valueObj.value}">${sortedKey}</option>`));
			}

			if(filters[k]){
				$(`#${k}.cql_filters`).val(filters[k]);
			}
		}
	}

	function retrieveTableColumns(fields){
		var columns = [];
		for(k in fields){
			columns.push({
				data: k
			});
		}
		return columns;
	}

	function generateRestArgs(){
		var cleanArgs = '';
		for(var propName in filters){
			if((filters[propName] != null) || (filters[propName] != undefined)){
				cleanArgs += '&' + propName + '=' + filters[propName];
			}
		}
		return cleanArgs;
	}

	function removeURLParameter(url, parameter){
		var urlparts = url.split('?');
		if(urlparts.length >= 2){
			var prefix = encodeURIComponent(parameter)+'=';
			var pars = urlparts[1].split(/[&;]/g);

			for(var i = pars.length; i-- > 0;){
				if(pars[i].includes(parameter)){
					pars.splice(i, 1);
				}
			}

			url = urlparts[0]+(pars.length > 0? '?' : '')+pars.join('&');
			return url;
		}
		else{
			return url;
		}
	}

	function createTable(){
		var $table = $('.assessment_table table');

		var $tableHeader = $('<thead><tr></tr></thead>');
		for(k in response_fields_to_table_map){
			$('tr', $tableHeader).append($(`<th>${response_fields_to_table_map[k].name}</th>`));
		}

		$table.append($tableHeader);
		$table.append($('tbody'));
		$table.append($('tfooter'));

		$table.show();
		table = $table.DataTable({
			columns : retrieveTableColumns(response_fields_to_table_map),
			dom: 'Bfrtp',
			buttons: [
				{
					extend: 'print',
					customize: function(win){
						$(win.document.body)
							.prepend(
								'<p>Data Source: see Source field in table</b></p>'
							);
						$(win.document.body).find( 'table' )
							.addClass( 'compact row-border' )
							.css( 'font-size', 'inherit' );
					}
				},
				{
					extend: 'pdfHtml5',
					orientation: 'landscape',
					pageSize: 'LEGAL',
					customize: function (doc){
						doc['header'] = 'Data Source: see Source field in table';
					}
				},
				{
					extend: 'excel',
					text: 'XLSX',
					messageTop: 'Data Source: see Source field in table'
				}
			],
			createdRow: function(row, data, dataIndex){
				// if(data.moderation_state != 'Approved'){
				// 	$(row).addClass('draft');
				// }

				$(row).on('click', function(){
					if(!$(this).hasClass('selected')){
						var wdpaId = parseInt($(this).find('td:first-child').text());
		
						$('.assessment_table table tbody tr').removeClass('selected');
						$(this).addClass('selected');
		
						if(assessmentMap){
							assessmentMap.showFeatureById(wdpaId);
						}
		
						$('html, body').animate({
							scrollTop: $("#assessment_map").offset().top - 100
						}, 1000);
					}
				});
			},
			rowCallback(row, data, displayNum, displayIndex, dataIndex){
				let i = 0;
				for(let k in response_fields_to_table_map){
					if(response_fields_to_table_map[k]['get_value']){
						jQuery(`td:eq(${i})`, row).html(response_fields_to_table_map[k]['get_value'](data));
					}
					i++;
				}
			}
		});

		var goToTobButton = new $.fn.dataTable.Buttons(
			table,
			{
				buttons: [
					{
						text: '<i class="fa fa-arrow-up"></i>',
						titleAttr: 'Go to top',
						className: 'button-scroller',
						action: function(e, dt, node, config){
							window.scrollTo(0, 0);
							return false;
						}
					}
				]
			}
		);
		goToTobButton.container().addClass('button-scroller-container');
		goToTobButton.container().appendTo($('.assessment_table'));
		
		$spinner.hide();
	}

	// function handleClickOnTableRow(){
	// 	$('.assessment_table table tbody tr').on('click', function(){
	// 		console.log($(this).attr('class'));
	// 		if(!$(this).hasClass('selected')){
	// 			var wdpaId = parseInt($(this).find('td:first-child').text());

	// 			$('.assessment_table table tbody tr').removeClass('selected');
	// 			$(this).addClass('selected');

	// 			if(assessmentMap){
	// 				assessmentMap.showFeatureById(wdpaId);
	// 			}

	// 			$('html, body').animate({
	// 				scrollTop: $("#assessment_map").offset().top - 100
	// 			}, 1000);
	// 		}
	// 	});
	// }

	function setTableData(){
		$spinner.show();
		var restArguments = generateRestArgs();
		
		var url = tableDataServiceUrl+"?format=json"+restArguments;
		$.getJSON(url,function(responseData){
			table.clear().draw();

			table.rows.add(responseData);
			
			let idx = 0, isColumnVisible;
			for(k in response_fields_to_table_map){
				if(response_fields_to_table_map[k].removable){
					isColumnVisible = false;
					table.column(idx).data().each((d, i) => {
						if(d){
							isColumnVisible = true;
						}
					});
					table.column(idx).visible(isColumnVisible);
				}
				idx++;
			}

			table.draw();

			Drupal.attachBehaviors($('table.dataTable').get(0));
			
			showRowsOnMap(responseData);

			$spinner.hide();

			// handleClickOnTableRow();

			updateFilters(responseData);
		});	
	}

	function showRowsOnMap(responseData){
		if(assessmentMap){
			// Initialize the variables used to display the right layer and the right viewport on the map.
			var wdpaIds = [];
			var iso3Codes = [];
			$.each(responseData,function(idx, obj){
				var thisWdpa = parseInt(obj.wdpa_id, 10);
				if(wdpaIds.indexOf(thisWdpa) === -1) wdpaIds.push(thisWdpa); //collect all wdpa IDs
				if(iso3Codes.indexOf(obj.iso3) === -1) iso3Codes.push(obj.iso3); //collect all countries to zoom to the group
			});

			// Move the map viewport to the properly display the bbox cotaining the selected countries.
			$.getJSON(
				getCountryBboxUrl+'?format=json&includemetadata=false&iso3codes='+iso3Codes.toString(),
				function(responseData){
					assessmentMap.showFeaturesByIdAndBbox(
						wdpaIds,
						jQuery.parseJSON(responseData.records[0].get_bbox_for_countries_dateline_safe)
					);
				}
			);			
		}
	}

	function initAssessmentTable(url, inputFields, map){
		tableDataServiceUrl = url,
		response_fields_to_table_map = inputFields;
		assessmentMap = map;
		setupFilters();
		createTable();
		setTableData();
		$('.assessment-table-tool-button').show();
	}

	biopamaAssessmentTable = {
		initAssessmentTable: initAssessmentTable,
		setTableData: setTableData
	}
})(jQuery);